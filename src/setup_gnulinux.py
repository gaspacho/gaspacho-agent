#!/usr/bin/env python

# Gaspacho-agent

from distutils.core import setup
from glob import glob

data_files = [('share/gaspacho-agent', ['resources/gnulinux/user.py',
                                        'resources/gnulinux/computer.py',
                                        'resources/gnulinux/context.py'])]
data_files.append(('/etc/xdg/autostart/', ['resources/gnulinux/gaspacho.desktop']))
setup(
    author='Emmanuel Garette',
    author_email='gnunux@gnunux.info',
    name='gaspacho-agent',
    version='0.1',
    description='Centralized configuration tools agent',
    url='http://www.gaspacho-project.net/',
    packages=['gaspacho_agent', 'gaspacho_agent.plugins'],
    package_dir={'gaspacho_agent.plugins': 'plugins/'},
    license='GPLv3', 
    data_files=data_files
)
 
