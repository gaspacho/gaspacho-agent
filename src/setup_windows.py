#!/bin/env python

# Copyright (C) 2011 Emmanuel Garette <gnunux@gnunux.info>
#               2011 Antoine Roger 
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA


from distutils.core import setup
import py2exe
import os, sys, glob


sys.path.append(os.path.abspath('.'))
sys.path.append(os.path.abspath('plugins/common/'))
sys.path.append(os.path.abspath('plugins/windows/'))

try:
    import modulefinder
    import win32com
    for p in win32com.__path__[1:]:
        modulefinder.AddPackagePath("win32com", p)
    for extra in ["win32com.shell"]: #,"win32com.mapi"
        __import__(extra)
        m = sys.modules[extra]
        for p in m.__path__[1:]:
            modulefinder.AddPackagePath(extra, p)

except ImportError:
    pass

dll1 = os.path.join(os.path.abspath('resources/windows/'), 'gaspacho.MFC')
dll2 = os.path.join(os.path.abspath('resources/windows/'), 'gaspacho.MSV')
dll3 = os.path.join(os.path.abspath('resources/windows/'), 'dlls')
# Ajout de ces chemins au %PATH%
os.environ['path']=';'.join([os.environ['path'], dll1, dll2, dll3])
# Ajout des DLLS aux 'data_files'
Py26dllsMFC = glob.glob(r"gaspacho.MFC\*.*")
Py26dllsMSV = glob.glob(r"gaspacho.MSV\*.*")
OtherDLLs = glob.glob(r"dlls\*.*")
data_files = [("", Py26dllsMSV),
              ("", Py26dllsMFC),
              ("", OtherDLLs),]

opts = {"py2exe": {
                    "packages":["encodings",],
                    "optimize":1,
                    #'bundle_files':1,
                    #"includes": "win32com,win32service,win32serviceutil,win32event, win32profile",
                    #'dll_excludes': [ "mswsock.dll","MSWSOCK.dll", "powrprof.dll" ]

}}

setup(name = "Agent Gaspacho",
      options = opts,
      description = "Agent Gaspsacho",
      author = "Antoine",
      service = [{'modules': 'resources.windows.services.gaspacho_service',}],
      #zipfile = None,
      data_files=data_files
)
