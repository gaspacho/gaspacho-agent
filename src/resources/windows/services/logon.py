# Copyright (C) 2011 Emmanuel Garette <gnunux@gnunux.info>
# 					 Antoine Roger 
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

import servicemanager
import win32com.client
import win32com.server.policy
import pythoncom
import win32security
import win32process
import win32profile
import win32con
import win32event
import win32ts
import os
import sys
# configure path to import from parent directory
main_path = os.path.abspath('../../../')
sys.path.append(main_path)
from gaspacho_agent.agent import start_agent
import sys

## from Sens.h
SENSGUID_PUBLISHER = "{5fee1bd6-5b9b-11d1-8dd2-00aa004abd5e}"
SENSGUID_EVENTCLASS_LOGON = "{d5978630-5b9f-11d1-8dd2-00aa004abd5e}"

## from EventSys.h
PROGID_EventSystem = "EventSystem.EventSystem"
PROGID_EventSubscription = "EventSystem.EventSubscription"

IID_ISensLogon = "{d597bab3-5b9f-11d1-8dd2-00aa004abd5e}"


class SensLogon(win32com.server.policy.DesignatedWrapPolicy):
    _com_interfaces_=[IID_ISensLogon]
    _public_methods_=[
        'Logon',
        'Logoff',
        'StartShell',
        'DisplayLock',
        'DisplayUnlock',
        'StartScreenSaver',
        'StopScreenSaver'
        ]

    def __init__(self):
        self._wrap_(self)

    def Logon(self, *args):
        logevent('Logon : %s'%[args])
        logon(args)
 
    def Logoff(self, *args):
        logevent('Logoff : %s'%[args])

    def StartShell(self, *args):
        logevent('StartShell : %s'%[args])
    
    def DisplayLock(self, *args):
        logevent('DisplayLock : %s'%[args])
    
    def DisplayUnlock(self, *args):
        logevent('DisplayUnlock : %s'%[args])
        # for easier tests
        logon(args)
    
    def StartScreenSaver(self, *args):
        logevent('StartScreenSaver : %s'%[args])
    
    def StopScreenSaver(self, *args):
        logevent('StopScreenSaver : %s'%[args])


def logevent(msg, evtid=0xF000):
    """log into windows event manager
    """
    servicemanager.LogMsg(
            servicemanager.EVENTLOG_INFORMATION_TYPE,
            evtid, #  generic message
            (msg, '')
            )

def register():
    pythoncom.CoInitializeEx(pythoncom.COINIT_MULTITHREADED)
    #modif
    logevent('Registring ISensLogon')

    sl=SensLogon()
    subscription_interface=pythoncom.WrapObject(sl)
    
    event_system=win32com.client.Dispatch(PROGID_EventSystem)
    
    event_subscription=win32com.client.Dispatch(PROGID_EventSubscription)
    event_subscription.EventClassID=SENSGUID_EVENTCLASS_LOGON
    event_subscription.PublisherID=SENSGUID_PUBLISHER
    event_subscription.SubscriptionName='Python subscription'
    event_subscription.SubscriberInterface=subscription_interface
    
    event_system.Store(PROGID_EventSubscription, event_subscription)
    
    pythoncom.PumpMessages()
    logevent('ISensLogon stopped')

    
def logon(args):
    #pythoncom.CoInitializeEx(pythoncom.COINIT_MULTITHREADED)
    #modif
    domain, username = args[0].split('\\')
    sess_id = get_sess_id(username)
    user_token = win32ts.WTSQueryUserToken(int(sess_id))
    user_id = win32security.GetTokenInformation(user_token,win32security.TokenUser)[0]
    my_sid = str(user_id).split(":")
    if my_sid[0] == "PySID":
        my_sid = my_sid[1]
    else:
        raise Exception('error during SID request')

    os.environ['USER']=username
    #os.environ['HOME']=homedir
    os.environ['UID']=user_id
    #os.environ['GID']=
    start_agent()
    
    

def get_sess_id(username):
    for sn in win32security.LsaEnumerateLogonSessions():
        sn_info = win32security.LsaGetLogonSessionData(sn)
        if sn_info['UserName'].lower() == username.lower():
            return sn_info['Session']


# vim: ts=4 sw=4 expandtab
