# Copyright (C) 2011 Emmanuel Garette <gnunux@gnunux.info>
#               2011 Antoine Roger 
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA


import win32serviceutil
import win32service
import win32event
import win32api
import servicemanager
import pythoncom

# the "magic" import
import logon

svcdeps=["EventLog"]


class MyService(win32serviceutil.ServiceFramework):
    """
       Definition du service Windows
    """
    _svc_name_ = 'GaspachoAgent'
    _svc_display_name_ = 'Gaspacho agent'
    _svc_deps_ = svcdeps


    def __init__(self, args):
        win32serviceutil.ServiceFramework.__init__(self, args)
        logon.logevent(self._svc_display_name_, servicemanager.PYS_SERVICE_STARTING)
        self.ReportServiceStatus(win32service.SERVICE_START_PENDING, waitHint=30000)
        
    def SvcDoRun(self):
        logon.logevent(self._svc_display_name_, servicemanager.PYS_SERVICE_STARTED)
        logon.register()
        self.ReportServiceStatus(win32service.SERVICE_STOPPED)

    def SvcStop(self):
        self.ReportServiceStatus(win32service.SERVICE_STOP_PENDING)
        # this does not seem to be the best way to stop PumpMessages()
        # it looks like it stops the whole service
        win32api.PostQuitMessage()
        # I think that's why "stop" events are not logged into Windows Event Manager
        self.ReportServiceStatus(win32service.SERVICE_STOPPED)
        # but it works...
        # one can also use win32api.PostThreadMessage
        # therefor, get the thread ID and call
        # win32api.PostThreadMessage(logon_thread_id, 18)

    #reboot/halt make a different call than 'net stop mytestservice'
    SvcShutdown = SvcStop

if __name__ == '__main__':
    win32serviceutil.HandleCommandLine(MyService)
