#!/usr/bin/env python

# Copyright (C) 2012 Emmanuel Garette <gnunux@gnunux.info>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

from gaspacho_agent.util import trace, logger
import pwd, sys, os
from gaspacho_agent.agent import start_agent

#GNU/Linux version of context

@trace
def context():
    logger.debug('context started')
    #for http://sourceforge.net/projects/pam-script/
    user = os.environ.get('PAM_USER', '')
    if user == '':
        #for http://freshmeat.net/projects/pam_script/
        if len(sys.argv) != 3:
            #FIXME: se ne sert pas du 2eme argument ...
            logger.error('must set two arguments: user and [open|close]')
            sys.exit(1)
        logger.info('open with %s' % sys.argv[2])
        user = sys.argv[1]
    if user == 'root':
        logger.debug('exit when trying to log with root user')
        sys.exit(0)
    if os.getuid() != 0:
        logger.error("run this script only in root")
        logger.debug("uid: %s"%os.getuid())
        sys.exit(1)
    info=pwd.getpwnam(user)
    homedir=info.pw_dir
    os.environ['USER']=user
    os.environ['HOME']=homedir
    os.environ['UID']=str(info.pw_uid)
    os.environ['GID']=str(info.pw_gid)
    logger.info('user %s logon with home %s'%(user, homedir))
    try:
        start_agent(conflevel='context')
    except Exception, e:
        logger.info(e)
        print e
        sys.exit(1)

if __name__ == '__main__':
    context()

# vim: ts=4 sw=4 expandtab
