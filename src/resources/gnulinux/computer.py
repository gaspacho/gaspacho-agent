#!/usr/bin/env python

# Copyright (C) 2011-2012 Emmanuel Garette <gnunux@gnunux.info>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

from gaspacho_agent.util import trace, logger
import pwd, sys, os
from gaspacho_agent.agent import start_agent

#GNU/Linux version of computer

@trace
def computer():
    logger.debug('computer started')
    if os.getuid() != 0:
        logger.error("run this script only in root")
        sys.exit(1)
    os.environ['UID'] = "0"
    os.environ['GID'] = "0"
    try:
        start_agent(conflevel='computer', install=True)
    except Exception, e:
        logger.info(e)
        print e
        sys.exit(1)

if __name__ == '__main__':
    computer()

# vim: ts=4 sw=4 expandtab

