# Copyright (C) 2010 Emmanuel Garette <gnunux@gnunux.info>
#FIXME: clientscribe
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

from os.path import join
from os import environ
import logging
from logging.handlers import RotatingFileHandler

from config import log_level
from util import is_ms_windows, is_gnu_linux

if is_ms_windows():
    from win32api import GetTempPath, GetUserName
    logdir = environ['WINDIR'] 
    tmpdir = GetTempPath()
    if tmpdir == None:
        tmpdir = 'C:\WINDOWS\Temp'
    username = GetUserName()
elif is_gnu_linux():
    logdir = '/var/log/gaspacho/'
    tmpdir = '/tmp'
    #username = environ['USER']
    username = 'mandriva'
else:
    raise Exception('Unknown environment')


# log file for gaspacho's agent
SERVICE_LOG = join(logdir, 'service.log')
# utilisateur.py
#UTILISATEUR_LOG = join(wa.GetTempPath(), 'cliscribe_utilisateur-%s.log'%wa.GetUserName())
USER_LOG = join(tmpdir, 'gaspacho-agent-%s.log'%username)

# vim: ts=4 sw=4 expandtab
