#!/usr/bin/env python

# Copyright (C) 2010-2012 Emmanuel Garette <gnunux@gnunux.info>
#               2011 Antoine Roger
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

import traceback
from subprocess import Popen, PIPE
from socket import gethostname
import httplib, urllib
try:
    import json
except:
    #for python 2.5
    import simplejson as json
import os
from glob import glob
from imp import find_module
#FIXME: see it:
#from gaspacho_agent.common.log import session_log
from gaspacho_agent.util import valid, get_osname, logger, trace
from gaspacho_agent.config import (gasp_server, gasp_port, gasp_tls,
                                  gasp_user_filename, gasp_context_filename,
                                  gasp_computer_filename, hostname)
from gaspacho_agent.package import install_packages

try:
    PLUGINS_PATH = os.path.join(find_module('gaspacho_agent')[1], 'plugins')
except Exception, e:
    logger.error('--- unable to find module gaspacho_agent.plugins ---')
    logger.debug(traceback.format_exc())
    logger.error('error: %s'%e)
    raise Exception('error: unable to find module gaspacho_agent.plugins: %s'%e)

@trace
def check_variable(variable):
    if not variable.has_key('key'):
        raise ValueError('variable has no key: %s' % str(variable))
    if not variable.has_key('value'):
        raise ValueError('variable has no value: %s' % str(variable))
    if not variable.has_key('type'):
        raise ValueError('variable has no type: %s' % str(variable))

@trace
def get_gasp(conflevel):
    """Loads from a json file or retrieves the gaspacho's configuration values
    (.gasp file). In gaspacho-agent.conf you have the ability to use a json file
    for debug purposes::

        gasp_user_filename = '/root/ALL.gasp'
        gasp_computer_filename = '/root/computer.gasp'

    :conflevel: user, context or computer

    """
    try:
        if conflevel == 'user' and gasp_user_filename:
            fname = gasp_user_filename
            logger.info('open %s'%fname)
            fh = file(fname, 'r')
            variables = json.load(fh, encoding='utf-8')
            fh.close()
        elif conflevel == 'context' and gasp_context_filename:
            fname = gasp_context_filename
            logger.info('open %s'%fname)
            fh = file(fname, 'r')
            variables = json.load(fh, encoding='utf-8')
            fh.close()
        elif conflevel == 'computer' and gasp_computer_filename:
            fname = gasp_computer_filename
            logger.info('open %s'%fname)
            fh = file(fname, 'r')
            variables = json.load(fh, encoding='utf-8')
            fh.close()
        else:
            parms = {}
            parms['conflevel'] = conflevel
            if conflevel == 'user' or conflevel == 'context':
                try:
                    parms['user'] = os.environ['USER']
                except:
                    raise Exception('No USER in environment')
            parms['os'] = get_osname()

            if hostname == None:
                parms['hostname'] = gethostname()
            else:
                parms['hostname'] = hostname
            params = urllib.urlencode(parms)
            headers = {"Content-type": "application/x-www-form-urlencoded",
                        "Accept": "text/plain"}
            if gasp_tls:
                proto = "https"
            else:
                proto = "http"
            logger.debug('get variables: "%s://%s:%s"' % (proto, gasp_server,
                        gasp_port))
            logger.debug('params: %s'%str(parms))
            if gasp_tls:
                conn = httplib.HTTPSConnection("%s:%s" % (gasp_server, gasp_port))
            else:
                conn = httplib.HTTPConnection("%s:%s" % (gasp_server, gasp_port))
            try:
                conn.request("POST", "/apply", params, headers)
            except Exception, e:
                raise Exception('unable to connect to %s://%s:%s: %s' % \
                            (proto, gasp_server, gasp_port, e))
            response = conn.getresponse()
            status = response.status
            reason = response.reason
            if status == '200' and reason == 'OK':
                raise('response not OK: %s %s' % (status, reason))
            #logger.debug('response: ' + str(response.read()))
            variables = json.load(response)
            logger.debug('variables: ' + str(variables))
            conn.close()
    except Exception,e:
        logger.error('error when trying to get variables:')
        logger.debug(traceback.format_exc())
        logger.error('error: %s'%e)
        raise Exception('error when trying to get variables')
    return variables

def get_plugins():
    """Collects the names of available plugins

    **it's only the names of the plugins (e.g. not the namespace)**
    """
    plugins = []
    try:
        for plugin_file in glob(os.path.join(PLUGINS_PATH, '[a-z]*.py')):
            plugin_name = os.path.basename(plugin_file)
            if plugin_name.startswith('gasp_'):
                #remove gasp_ in name
                plugin_name = plugin_name[5:]
                #remove extension
                plugin_name = os.path.splitext(plugin_name)[0]
                plugins.append(plugin_name)
    except Exception,e:
        logger.error('error when trying to get plugins:')
        logger.debug(traceback.format_exc())
        logger.error('error: %s'%e)
        raise Exception('error when trying to get plugins')
    return plugins

def launch_plugin(plugin_name, plugin_namespace, targets, conflevel):
    """Executes the plugin's regular methods from the plugin's API

    :plugin_namespace: __import__ result in it
    :targets: variables and path descriptions that corresponds to this plugin
    """
    try:
        assert plugin_namespace.gasp_version == "0.1"
    except:
        logger.error('  - plugin %s version not compatible' % plugin_name)
        del(plugin)
        return

    plugin = plugin_namespace.Apply(conflevel)

    if not plugin.is_compatible():
        if targets:
            logger.error('  - plugin %s is not compatible with this conflevel' % (plugin_name, conflevel))
        del(plugin)
        return
    plugin.initialize()
    if targets == None:
        logger.debug('  - no target')
        plugin.finalize_without_target()
        del(plugin)
        return
    for target in targets:
        path = os.path.expandvars(target['path'])
        logger.debug('  + open path %s' % path)
        #instanciate this class with path
        plugin.open(path)
        for variable in target['variables']:
            check_variable(variable)
            info = variable.get('info', None)
            logger.debug('    info: %s key: %s value: %s and extension: %s' % \
                (info, variable['key'], variable['value'], variable['type']))
            #value could be IGNORE, but this target must not set in json
            if variable['value'] == 'SUPPR':
                logger.debug('    SUPPR')
                try:
                    plugin.suppr(variable['key'], info)
                except NotImplemented:
                    logger.error('error: delete not implemented' \
                                 + ' for extension: %s' % extension)

            else:
                value = valid(variable['value'], variable['type'])
                plugin.set(variable['key'], value, info)
        plugin.close()
    plugin.finalize()
    del(plugin)

@trace
def execvars_hook(execvars):
    """
    :execvars: object that represents the target configuration file
    """
    for target in execvars:
        if target['path'] != None:
            raise Exception('path must be None for exec')
        for variable in target['variables']:
            exec_type = variable['type']
            if exec_type == 'shell':
                if variable.get('info', None) != None:
                    raise Exception('info for exec must be None')
                # makes and launches a commandline
                cmd = [variable['key']]
                cmd.extend(variable['value'].split())
                logger.debug('exec command: %s' % ' '.join(cmd))
                process = Popen(cmd, stdout=PIPE, stderr=PIPE, stdin=PIPE)
                stdout, stderr = process.communicate()
                logger.debug(stdout)
                if process.returncode:
                    logger.debug('error in %s: %s' % (' '.join(cmd), stderr))
            else:
                logger.error('unknown exec_type %s' % exec_type)

@trace
def start_agent(conflevel, install=False):
    """workstation agent's entry point

    :conflevel: user or computer
    """
    if conflevel not in ['user', 'computer', 'context']:
        raise Exception('Unknown conflevel %s'%conflevel)
    gasp = get_gasp(conflevel)
    variables = gasp['plugins']
    if install:
        packages = gasp['packages']
        install_packages(packages)
    plugins = get_plugins()
    try:
        logger.debug('parse variables')
        # an extension (variable.key) have to be linked to
        # a plugin with the same name
        for extension in list(set(variables.keys()) - set(plugins)):
            if extension not in ['preexec', 'postexec']:
                logger.error('No plugin for %s !' % extension)

        execvars = variables.get('preexec', None)
        if execvars != None:
            execvars_hook(execvars)

        for extension in plugins:
            logger.debug(' - extension: ' + extension)
            try:
                # manually imports from a python file in order to load it
                # into the 'plugin' namespace
                plugin_namespace = __import__('gaspacho_agent.plugins.gasp_%s' % \
                                             extension, None, None, [extension])
            except ImportError, e:
                logger.debug(traceback.format_exc())
                logger.error('unable to import extension %s: %s' % (extension,
                                e))
                continue
            except Exception, e:
                logger.debug(traceback.format_exc())
                raise Exception(e)
            try:
                launch_plugin(extension, plugin_namespace, variables.get(extension, None),
                            conflevel)
            except Exception, e:
                logger.debug(traceback.format_exc())
                logger.info("problem with %s's plugin" % extension)

        execvars = variables.get('postexec', None)
        if execvars != None:
            execvars_hook(execvars)

    except Exception,e:
        logger.debug(traceback.format_exc())
        raise Exception('error: %s'%e)
    logger.info('agent ended')

# vim: ts=4 sw=4 expandtab
