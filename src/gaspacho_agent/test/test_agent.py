try:
    import json
except:
    #for python 2.5
    import simplejson as json
from os.path import dirname, abspath, join

from gaspacho_agent import agent
from py.test import raises


here = dirname(abspath(__file__))
data = join(here, "data")

check_variable_file = join(data, "check_variable.gasp")

def test_check_variable():
    # everything ok
    fh = file(check_variable_file, 'r')
    variables = json.load(fh, encoding='utf-8')
    fh.close()
    agent.check_variable(variables['xcu3'][0]['variables'][0])
    # raises 

    del(variables["xcu3"][0]['variables'][0]['key'])
    raises(ValueError, "agent.check_variable(variables['xcu3'][0]['variables'][0])")


