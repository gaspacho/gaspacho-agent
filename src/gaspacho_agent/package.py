#!/usr/bin/env python

# Copyright (C) 2012 Emmanuel Garette <gnunux@gnunux.info>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

import subprocess
from gaspacho_agent.util import logger

DPKG='/usr/bin/dpkg'
APT='/usr/bin/apt-get'

env = {'DEBIAN_FRONTEND': 'noninteractive'}

def run_cmd(cmd):
    process = subprocess.Popen(cmd, env=env)
    process.communicate()
    return process.returncode

def install_packages(packages):
    tpackages = []

    for package in packages:
        ret = run_cmd([DPKG, '-l', package])
        if ret != 0:
            tpackages.append(package)

    if tpackages != []:
        logger.debug(run_cmd([APT, 'update']))
        for tpackage in tpackages:
            logger.debug(run_cmd([APT, '-q', '-y', 'install', tpackage]))

# vim: ts=4 sw=4 expandtab
