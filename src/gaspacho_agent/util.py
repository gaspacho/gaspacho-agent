# Copyright (C) 2010 Emmanuel Garette <gnunux@gnunux.info>
#FIXME: clientscribe
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

from os import mkdir
from os.path import isdir, curdir
from formencode import validators
import logging
from logging.handlers import SysLogHandler
from gaspacho_agent.config import log_level, log_file
import platform


def trace(func):
    """This is a decorator which can be used to trace functions
    """
    def newFunc(*args, **kwargs):
        logger.debug("-> entering %s(%s, %s)"% (func.__name__, str(args), str(kwargs)))
        return func(*args, **kwargs)
    newFunc.__name__ = func.__name__
    newFunc.__doc__ = func.__doc__
    newFunc.__dict__.update(func.__dict__)
    return newFunc

def get_logger(loggername):
    """logging facilites
    """
    LEVELS = {'debug': logging.DEBUG,
              'info': logging.INFO,
              'warning': logging.WARNING,
              'error': logging.ERROR,
              'critical': logging.CRITICAL}

    if not log_level in LEVELS:
        print "%s is not a valid log_level"%log_level
        exit(1)
    logging_format = logging.Formatter('%(name)s[%(process)d] %(levelname)s "%(funcName)s" %(message)s')
    gasp_logger = logging.getLogger(loggername)
    gasp_logger.setLevel(LEVELS[log_level])
    handler = SysLogHandler(address="/dev/log")
    handler.setFormatter(logging_format)
    gasp_logger.addHandler(handler)
    return gasp_logger

logger = get_logger('gaspacho-agent')

@trace
def makedirs(name, uid=None, gid=None, mode=0700):
    #this is adaptation of os.makedirs for add uid and gid
    if ( uid != None or gid !=None ) and ( uid == None or gid == None ):
        raise ValueError("uid and gid must be set together")
    if is_gnu_linux():
        import posixpath as path
    elif is_ms_windows():
        import ntpath as path
    else:
        raise Exception('unkown platform')
    head, tail = path.split(name)
    if not tail:
        head, tail = path.split(head)
    if head and tail and not path.exists(head):
        try:
            makedirs(head, uid=uid, gid=gid, mode=mode)
        except OSError, e:
            if e.errno != errno.EEXIST:
                raise
        if tail == curdir:
            return
    mkdir(name, mode)
    if uid:
        chown(name, uid, gid)
    #except Exception, e:
    #    if type(name) == unicode:
    #        name = unicodedata.normalize("NFKD", name).encode("ascii", "ignore" )
    #    logging.error('Director not created "%s" %s'%(name, e))
    #    logging.debug('Error %s'%traceback.format_exc())

@trace
def chown(filename, uid, gid):
    if is_gnu_linux():
        from os import chown as gl_chown
        gl_chown(filename, uid, gid)
    if is_ms_windows():
        from win32security import ACL, ACL_REVISION_DS, OBJECT_INHERIT_ACE, \
                        DACL_SECURITY_INFORMATION, CONTAINER_INHERIT_ACE, \
                        SetFileSecurity, GetFileSecurity
        from win32file import FILE_ALL_ACCESS
        #FIXME: only support uid and just add uid
        acl = ACL()
        sd = GetFileSecurity(filename, DACL_SECURITY_INFORMATION)
        if isdir(filename):
            acl.AddAccessAllowedAceEx(ACL_REVISION_DS,
                        OBJECT_INHERIT_ACE|CONTAINER_INHERIT_ACE,
                        FILE_ALL_ACCESS, uid)
        else:
            acl.AddAccessAllowedAce(FILE_ALL_ACCESS, uid)
        sd.SetSecurityDescriptorDacl(1, acl, 0)
        SetFileSecurity(filename, DACL_SECURITY_INFORMATION, sd)

def is_gnu_linux():
    return platform.system() == 'Linux'

def is_ms_windows():
    return platform.system() == 'Windows'

@trace
def get_osname():
    plat = platform.linux_distribution()
    logger.debug('platform: %s'%str(plat))
    name = plat[0]
    version = plat[1]
    if name == 'Mandriva Linux':
        name = 'mandriva'
    platform_name = "%s_%s" % (name.lower(), version.lower())
    logger.debug('platform name: %s'%platform_name)
    return platform_name

def valid(value, typ):
    if typ == 'unicode':
        #if value is empty, return '' not u''
        if value == '':
            return u''
        validator = validators.String()
    elif typ == 'boolean':
        validator = validators.StringBoolean()
    elif typ == 'integer':
        validator = validators.Int(not_empty=True)
    elif typ == 'list':
        raise Exception('not supported')
    else:
        raise Exception('unknown type: %s'%typ)
    return validator.to_python(value)

# vim: ts=4 sw=4 expandtab
