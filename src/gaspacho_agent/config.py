# Copyright (C) 2010 Emmanuel Garette <gnunux@gnunux.info>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

from configobj import ConfigObj

#FIXME:
conffile = "/etc/gaspacho-agent/gaspacho-agent.conf"
#conffile = 'c:\gaspacho-agent.conf'

try:
    config = ConfigObj(conffile)
except Exception, e:
    import traceback
    logger.error('--- unable to load configuration file %s ---' % conffile)
    logger.debug(traceback.format_exc())
    raise Exception('error: %s'%e)

try:
    gasp_server = config['gasp_server']
except Exception, e:
    import traceback
    logger.error('--- unable to find server address in configuration file %s ---' % conffile)
    logger.debug(traceback.format_exc())
    logger.error('error: %s'%e)
    raise Exception('error: %s'%e)

#loglevel must be in 'critical', 'error', 'warning', 'info' or 'debug'
log_level = config.get('log_level', 'error')
log_file = config.get('log_file', '/var/log/gaspacho_agent/error.log')

gasp_port = str(config.get('gasp_port', '8080'))
gasp_tls = str(config.get('gasp_tls', False))
gasp_user_filename=config.get('gasp_user_filename', None)
gasp_computer_filename=config.get('gasp_computer_filename', None)
gasp_context_filename=config.get('gasp_context_filename', None)

hostname = config.get('hostname', None)

# vim: ts=4 sw=4 expandtab
