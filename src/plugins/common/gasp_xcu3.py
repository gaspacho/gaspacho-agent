# Copyright (C) 2011 Emmanuel Garette <gnunux@gnunux.info>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

try:
    from gaspacho_agent.plugins.gasp_xcu import Apply as Xcu
    from gaspacho_agent.plugins.gasp_xcu import Element
except:
    compatibility=False
else:
    compatibility=True
from gaspacho_agent.util import logger, trace

gasp_version = "0.1"

class Apply(Xcu):
    @trace
    def __init__(self, conflevel):
        Xcu.__init__(self, conflevel, qname='path')

    @trace
    def is_compatible(self):
        return compatibility

    @trace
    def _get_prop(self, var, item):
        key = self._get_qname('oor', 'path')
        namekey = self._get_qname('oor', 'name')
        for children in self.root.getchildren():
            node = children.get(key, None)
            if node == item:
                for child in children:
                    prop = child.get(namekey, None)
                    if prop == var:
                        return child
        #if no node, add one
        node = Element('item')
        node.attrib[self._get_qname('oor', 'path')] = item
        self.root.append(node)   
        #with a prop
        prop = Element('prop')
        prop.attrib[self._get_qname('oor', 'name')] = var
        #FIXME: i don't understand what fuse means
        prop.attrib[self._get_qname('oor', 'op')] = 'fuse'
        node.append(prop)
        #and a value
        pval = Element('value')
        prop.append(pval)
        return prop

    @trace
    def set(self, key, value, info):
        #value should be like that: Inet/Settings
        item = '/org.openoffice.%s'%info
        typ = type(value)
        #if typ == 'list':
        #    val = '\n'.join(val)
        xcutyp = {unicode: 'xs:string',
               bool: 'xs:boolean',
               int: 'xs:int'}.get(typ, None)
        if xcutyp == None:
            raise TypeError('unknown type: %s ' % str(typ))
        prop = self._get_prop(key, item)

        if self._getattr([prop], 'fuse', 'op') == None:
            raise Exception('%s already exist but with different type'%key)

        pval = prop[0]
        if pval.tag != 'value':
            raise Exception('should be value')
        #set value:
        pval.text = str(value)
        key = self._get_qname('oor', 'path')
        namekey = self._get_qname('oor', 'name')

