# Copyright (C) 2010 Emmanuel Garette <gnunux@gnunux.info>
#FIXME: clientscribe
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

import re
from os.path import isfile, dirname, isdir
from os import environ
from gaspacho_agent.util import makedirs, chown
from gaspacho_agent.util import logger, trace

gasp_version = "0.1"

class Apply:
    @trace
    def __init__(self, conflevel):
        self.conflevel = conflevel

    @trace
    def is_compatible(self):
        return True

    @trace
    def initialize(self):
        self.prefs = None
        #regexp to import file
        self.regexp = r'^(\S+): (\S+)$'
        #pattern to save file
        self.pattern = '%s: %s\n'
        #add quote for key (ie: "key_name": "value")
        self.key_add_quote = False
        #boolean are True or False by default
        #this var can change default value
        self.boolean = {}
        self.fileheader = None
        self.filename = None
        self.uid = int(environ['UID'])
        self.gid = int(environ['GID'])

    @trace
    def open(self, path):
        if self.filename != None:
            raise Exception('use close() before it')
        self.filename = path
        self._load_file()

    @trace
    def suppr(self, key, info):
        """
        Delete an item
        Use del(pref[var])

        If key not exist, just pass
        """
        if self.filename == None:
            raise Exception('use open() before it')
        if info != None:
            raise Exception('info must not be None')
        if self.key_add_quote == True:
            key = '"%s"' % key
        try:
            del self.prefs[key]
        except KeyError:
            pass

    @trace
    def _load_file(self):
        dir_name = dirname(self.filename)
        if not isdir(dir_name):
            if self.conflevel == 'user':
                mode = 0700
            else:
                mode = 0755
            makedirs(dirname(self.filename), uid=self.uid, gid=self.gid,
                        mode=mode)
        self.prefs = {}
        if isfile(self.filename):
            prefs = file(self.filename, 'r')
            compile = re.compile(self.regexp, re.MULTILINE)
            for key, value in compile.findall(prefs.read()):
                self.prefs[key] = value
            prefs.close()

    @trace
    def set(self, key, value, info):
        if self.filename == None:
            raise Exception('use open() before it')
        if self.prefs == None:
            raise Exception('use _load_file() before it')
        if info != None:
            raise ValueError('info must not be set')
        typ = type(value)
        if typ == unicode:
            if not (value.startswith('"') and value.endswith('"')):
                value = '"%s"' % value
        elif typ == bool:
            value = self.boolean.get(value, value)
        elif typ == int:
            pass
        else:
            raise TypeError('unknown type: %s ' % str(typ))
        if self.key_add_quote == True:
            key = '"%s"' % key
        self.prefs[key] = value

    @trace
    def close(self):
        """
        Save preference in self.filename or, optionnaly, in filename
        """
        if self.filename == None:
            raise Exception('already close')
        content = []
        save_file = file(self.filename, 'w')
        if self.fileheader != None:
            save_file.write(self.fileheader)
        for key, value in self.prefs.iteritems():
            save_file.write(self.pattern % (key, value))
        save_file.close()
        chown(self.filename, self.uid, self.gid)
        self.filename = None
        self.prefs = None

    @trace
    def finalize(self):
        pass

    @trace
    def finalize_without_target(self):
        pass

# vim: ts=4 sw=4 expandtab
