# Copyright (C) 2010 Emmanuel Garette <gnunux@gnunux.info>
#                    Antoine Roger
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

from gaspacho_agent.plugins.gasp_key_value import Apply as KeyValue
from gaspacho_agent.util import logger, trace

gasp_version = "0.1"

class Apply(KeyValue):
    @trace
    def initialize(self):
        KeyValue.initialize(self)
        self.key_add_quote = True
        self.boolean = {True: 'true', False: 'false'}
        self.fileheader = '//\n'
        self.info = None

    @trace
    def is_compatible(self):
        return True

    @trace
    def open(self, path):
        #file not load now, need "info" value in variable
        if self.filename != None:
            raise Exception('use close() before it')
        self.filename = path

    @trace
    def set(self, key, value, info):
        #load file if not already loaded
        if self.prefs == None:
            if info == None:
                self.info = 'lockPref'
            else:
                self.info = info
            reg = r'"(?:[^"\\]|\\")*"'
            self.regexp = r"%s\s*\((%s)\s*,\s*(%s|[^ )]+)\s*\)" % \
                            (self.info, reg, reg)
            self.pattern = '%s(%%s, %%s);\n' % self.info
            self._load_file()
        elif self.info != info:
            if not (info == None and self.info == 'lockPref'):
                raise Exception(
                            'vars for this file need have same info attribut')

        return KeyValue.set(self, key, value, None)
        
# vim: ts=4 sw=4 expandtab
