# Copyright (C) 2010 Emmanuel Garette <gnunux@gnunux.info>
#FIXME: clientscribe
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

from gaspacho_agent.plugins.gasp_key_value import Apply as KeyValue
from gaspacho_agent.util import logger, trace

gasp_version = "0.1"

class Apply(KeyValue):
    @trace
    def initialize(self):
        KeyValue.initialize(self)
        #regexp to import file
        self.regexp = r'^(\S+) = (\S+)$'
        #pattern to save file
        self.pattern = '%s = %s\n'

# vim: ts=4 sw=4 expandtab
