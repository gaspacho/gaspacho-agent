# Copyright (C) 2011 Emmanuel Garette <gnunux@gnunux.info>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

#OpenOffice.org Registry Format (OOR)
#informations: http://util.openoffice.org/common/configuration/oor-document-format.html

import xml.etree.ElementTree as ET
from xml.etree.ElementTree import Element, ElementTree
from os.path import dirname, isdir, isfile
from os import environ
from gaspacho_agent.util import makedirs, chown
from gaspacho_agent.util import logger, trace

try:
    #for python 1.7
    register_namespace = ET.register_namespace
except AttributeError:
    #for python since 1.5
    def register_namespace(prefix, uri):
        ET._namespace_map[uri] = prefix

gasp_version = "0.1"

class Apply:
    @trace
    def __init__(self, conflevel, section_separator='/', qname='name'):
        self.section_separator = section_separator
        self.qname = qname
    
    @trace
    def is_compatible(self):
        return True

    @trace
    def initialize(self):
        self.path = None
        register_namespace('oor', 'http://openoffice.org/2001/registry')
        register_namespace('xs', 'http://www.w3.org/2001/XMLSchema')
        register_namespace('xsi', 'http://www.w3.org/2001/XMLSchema-instance')
        self.xcu = None
        self.root = None
        self.uid = int(environ['UID'])
        self.gid = int(environ['GID'])

    @trace
    def _get_qname(self, name, attr):
        NameSpace = {'oor': 'http://openoffice.org/2001/registry'}
        return ET.QName(NameSpace[name], attr)

    @trace
    def open(self, path):
        if self.path != None:
            raise Exception('use close() before it')
        self.path = path
        #try:
        if isfile(path):
            self.xcu = ET.parse(self.path)
            self.root = self.xcu.getroot()
        else:
            if self.qname != 'name':
                open_component = 'component-data'
            else:
                open_component = 'items'
            self.root = Element(self._get_qname('oor', open_component))
            self.xcu = ElementTree(self.root)

    @trace
    def _getattr(self, children, name, qname):
        key = self._get_qname('oor', qname)
        for child in children:
            node = child.get(key, None)
            if node == name:
                return child
        return None

    @trace
    def set(self, key, value, info):
        if self.qname != 'name':
            raise Exception('Unsupported qname value: %s'%self.qname)
        #value should be like that: Inet/Settings

        values = info.split(self.section_separator)
        root = [self.root]
        for index, section in enumerate(values):
            if index == 0:
                children = [self.root]
            else:
                children = root.getchildren()
            if children == []:
                #if no node
                node = Element('node')
                node.attrib[self._get_qname('oor', self.qname)] = section
                root.append(node)
                root = node
            else:
                nroot = self._getattr(children, section, self.qname)
                #if root has no name
                if nroot == None and index == 0:
                    if root[0].attrib.has_key(self._get_qname('oor', 'name')):
                        raise Exception('root node has a differente name: %s instead of %s'%(root[0].attrib[self._get_qname('oor', 'name')], name))
                    root[0].attrib[self._get_qname('oor', 'name')] = section
                    root[0].attrib[self._get_qname('oor', 'package')] = 'org.openoffice'
                    nroot = root[0]
                root = nroot
        typ = type(value)
        xcutyp = {unicode: 'xs:string',
               bool: 'xs:boolean',
               int: 'xs:int'}.get(typ, None)
        if xcutyp == None:
            raise TypeError('unknown type: %s ' % str(typ))
        #if typ == 'list':
        #    val = '\n'.join(val)
        #unsupported type:
        #xs:short xs:long xs:double xs:hexBinary oor:any oor:nillable
        prop = self._getattr(root, key, 'name')
        #if prop not exist
        if prop == None:
            prop = Element('prop')
            prop.attrib[self._get_qname('oor', 'name')] = key
            if self.qname == 'name':
                prop.attrib[self._get_qname('oor', 'type')] = xcutyp
            elif self.qname == 'path':
                #FIXME: i don't understand what fuse means
                prop.attrib[self._get_qname('oor', 'op')] = 'fuse'
            else:
                raise Exception('Unsupported qname type: %s'%self.qname)

            pval = Element('value')
            prop.append(pval)
            root.append(prop)
        else:
            if ( self.qname == 'name' and \
                        self._getattr([prop], xcutyp, 'type') == None ) or \
                        ( self.qname == 'path' and \
                        self._getattr([prop], 'fuse', 'op') == None ):
                raise Exception('%s already exist but with different type'%key)
            pval = prop[0]
            if pval.tag != 'value':
                raise Exception('should be value')
        #set value:
        pval.text = str(value)

    @trace
    def suppr(self, key, info):
        if self.path == None:
            raise Exception('use open() before it')
        raise NotImplemented('unsupported')

    @trace
    def close(self):
        if self.path == None:
            raise Exception('already close')
        dirn = dirname(self.path)
        if not isdir(dirn):
            makedirs(dirn, uid=self.uid, gid=self.gid)
        #fixup xmlns:xs namespace
        #self.root.set("xmlns:" + 'xsi', 'http://www.w3.org/2001/XMLSchema-instance')
        self.root.set("xmlns:" + 'xs', 'http://www.w3.org/2001/XMLSchema')
        #write and change right
        self.xcu.write(self.path, encoding='UTF-8')
        chown(self.path, self.uid, self.gid)
        self.path = None

    @trace
    def finalize_without_target(self):
        pass

    @trace
    def finalize(self):
        pass

# vim: ts=4 sw=4 expandtab
