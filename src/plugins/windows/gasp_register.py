# Copyright (C) 2011 Emmanuel Garette <gnunux@gnunux.info>
#					 Antoine Roger
#FIXME: clientscribe
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

import _winreg
import win32security
import win32con
import win32api
import win32security
import win32process
import os
import sys
# configure path to import from parent directory
#common_path = os.path.abspath('../common/')
#sys.path.append(common_path)
from log import session_log
from gaspacho_agent.util import logger, trace

@trace
def clean(conflevel):
    pass

class Apply:

    @trace
    def __init__(self, conflevel):
        self.reg_types = {
            'integer': _winreg.REG_DWORD,
            'unicode': _winreg.REG_SZ,
            'boolean': _winreg.REG_DWORD,
        }
        if conflevel == 'user':
            self.hkey = _winreg.HKEY_USERS
        elif conflevel == 'computer':
            self.hkey = _winreg.HKEY_LOCAL_MACHINE
        else:
            raise ValueError('unknown conflevel: %s'%conflevel)
        self.okey = None
        #_winreg.CreateKey(hkey, path)
        #self.okey = _winreg.OpenKeyEx(hkey, path, 0, _winreg.KEY_ALL_ACCESS)

    def initialize(self):
        pass

    @trace
    def open(self, path):
        #logger = session_log()
        if self.okey != None:
            raise Exception('use close() before it')

        my_sid = os.environ["UID"]
        logger.debug("sid:" + my_sid)
        path = my_sid + "\\" + path
            
        _winreg.CreateKey(self.hkey, path.replace('\\','\\\\'))
        self.okey = _winreg.OpenKeyEx(self.hkey, path, 0,
                                        _winreg.KEY_ALL_ACCESS)

    @trace
    def suppr(self, key, info):
        if self.okey == None:
            raise Exception('use open() before it')
        if info != None:
            raise Exception('info must be None')
        try:
            _winreg.DeleteValue(self.okey, key)
        except WindowsError:
            pass

    @trace
    def set(self, key, value, info):
        if self.okey == None:
            raise Exception('use open() before it')
        if info != None:
            raise Exception('info must be None')
        typ, val = value
        if typ not in ['unicode', 'boolean', 'integer']:
            raise TypeError('Type of must be unicode, boolean or integer, not %s'%typ)
        typ = self.reg_types.get(typ, None)
        if typ == None:
            raise Exception('unknown typ: %s' % typ)
        _winreg.SetValueEx(self.okey, key, 0, typ, val)

    @trace
    def close(self):
        if self.okey == None:
            raise Exception('already close')
        try: 
            self.okey.Close()
        except:
            pass
        self.okey = None

    @trace
    def finalize_without_target(self):
        pass

    @trace
    def finalize(self):
        pass



##FIXME: dans apply ?
#    def regedit(self, key, keytype, value):
#        keytype = REG_TYPES[keytype]
#
#        # a\b\c\d\e => a, b\c\d, e
#        key = key.rstrip("\\")
#        hkey, keypath = key.split("\\", 1)
##pourquoi ?
##        if hkey == "HKEY_CURRENT_USER":
##            hkey = "HKEY_USERS"
##            keypath = "%s\\%s" % (self.sid, keypath)
#        keypath, keyname = keypath.rsplit("\\", 1)
#
#        reg = {
#            'hkey': hkey,
#            'keypath': keypath,
#            'keyname': keyname,
#            'keytype': keytype,
#            'value': value,
#        }
#        return reg

# vim: ts=4 sw=4 expandtab
