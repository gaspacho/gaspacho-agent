# Copyright (C) 2011 Emmanuel Garette <gnunux@gnunux.info>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

from os.path import isfile, isdir, dirname

from gaspacho_agent.util import logger, trace

gasp_version = "0.1"

class Apply():
    @trace
    def __init__(self, conflevel=None):
        self.conflevel = conflevel

    def is_compatible(self):
        return True

    def initialize(self):
        self.file = None
        self.path = None

    @trace
    def open(self, path):
        if self.file != None:
            raise Exception('use close() before it')
        if isfile(path):
            fh = open(path, 'r')
            self.file = fh.readlines()
            fh.close()
        else:
            self.file = []
        self.path = path

    @trace
    def suppr(self, key, info):
        if self.file == None:
            raise Exception('use open() before it')
        raise NotImplemented('unsupported')

    @trace
    def _mod_var_file(self, string, value):
        #var must have 2 lines:
        # - readonly string=value
        # - export string
        content = 'readonly %s=%s 2> /dev/null\n'%(string, value)
        content_export = 'export %s\n'%string
        for line_index in range(0, len(self.file)):
            line = self.file[line_index].strip().rstrip()
            if line.startswith('%s='%string) or ' %s='%string in line:
                self.file[line_index] = content
                if content_export not in self.file:
                    self.file.append(content_export)
                return
        self.file.append(content)
        if content_export not in self.file:
            self.file.append(content_export)

    @trace
    def _mod_line_file(self, string):
        #add line if not found
        for line_index in range(0, len(self.file)):
            line = self.file[line_index].strip().rstrip()
            if line == string.strip().rstrip():
                return
        self.file.append('%s\n'%(string))

    @trace
    def set(self, key, value, info):
        #info must be None or a file name
        #var only set if this file exists
        if self.file == None:
            raise Exception('use open() before it')
        self.info = info
        if self.info == u'var':
            self._mod_var_file(key, value)
        elif self.info == u'line':
            self._mod_line_file(key)
        else:
            raise Exception('unknown info: %s'%self.info)

    @trace
    def close(self):
        if self.file == None:
            raise Exception('already close')
        if not isdir(dirname(self.path)):
            raise Exception('parent directory must already exists for %s' % self.path)
        fh = open(self.path, 'w')
        fh.writelines(self.file)
        self.file = None
        self.path = None

    @trace
    def finalize_without_target(self):
        pass

    @trace
    def finalize(self):
        pass

# vim: ts=4 sw=4 expandtab
