# Copyright (C) 2010 Emmanuel Garette <gnunux@gnunux.info>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

from os.path import join, isdir, expandvars, isfile
from os import environ, system, walk, chmod
from subprocess import Popen, PIPE
from shutil import rmtree

from gaspacho_agent.util import makedirs, chown
from gaspacho_agent.util import logger, trace

GASPACHO_ROOT = '/etc/gaspacho-agent/users'
GCONF_BIN = '/usr/bin/gconftool-2'

gasp_version = "0.1"

@trace
class Apply:
    @trace
    def __init__(self, conflevel):
        self.conflevel = conflevel
        gaspacho_home = expandvars(join(GASPACHO_ROOT, '$USER'))
        self.mandatory_path = join(gaspacho_home, 'gconf')

    @trace
    def is_compatible(self):
        if self.conflevel != 'user':
            return False
        if not isfile(GCONF_BIN):
            return False
        return True

    def clean(self):
        if isdir(self.mandatory_path):
            rmtree(self.mandatory_path)

    @trace
    def initialize(self):
        self.clean()
        makedirs(self.mandatory_path, mode=0755)
        #gdm issues, need gconf directory
        gdm_dir = join(GASPACHO_ROOT, 'gdm', 'gconf')
        if not isdir(gdm_dir):
            makedirs(gdm_dir, mode=0755)

        self.path = None
        #set good right
        uid=0
        gid=int(environ['GID'])
        dirmode=0750
        chown(self.mandatory_path, uid, gid)
        chmod(self.mandatory_path, dirmode)

    @trace
    def open(self, path):
        if self.path != None:
            raise Exception('use close() before it')
        self.path = path

    @trace
    def suppr(self, key):
        if self.path == None:
            raise Exception('use open() before it')
        raise NotImplemented('unsupported')

    @trace
    def set(self, key, value, info):
        if self.path == None:
            raise Exception('use open() before it')
        if info != None:
            raise ValueError('info must not be set')
        application = [GCONF_BIN, '--direct']
        application.extend(['--config-source', 'xml:readwrite:%s' % 
            self.mandatory_path])
        typ = type(value)
        if typ == unicode:
            gconf_type='string'
        elif typ == int:
            gconf_type='int'
        elif typ == bool:
            gconf_type='bool'
            value = str(value).lower()
        else:
            raise TypeError('unknown type: %s ' % str(typ))
        path = str(join(self.path, key))
        cmd = application
        cmd.extend(['--type', gconf_type, '--set', path, str(value)])
        process = Popen(cmd, stdout=PIPE, stderr=PIPE, stdin=PIPE)
        stdout, stderr = process.communicate()
        if process.returncode:
            raise Exception('error in %s', ' '.join(cmd))

    @trace
    def close(self):
        if self.path == None:
            raise Exception('already close')
        self.path = None

    @trace
    def finalize_without_target(self):
        pass

    @trace
    def finalize(self):
        if self.path != None:
            raise Exception('use close() before it')
        system(GCONF_BIN + ' --shutdown')
        uid=0
        gid=int(environ['GID'])
        dirmode=0750
        filemode=0640
        for root, dirs, files in walk(self.mandatory_path):
            chown(root, uid, gid)
            chmod(root, dirmode)
            for file in files:
                filename = join(root, file)
                chown(filename, uid, gid)
                chmod(filename, filemode)

# vim: ts=4 sw=4 expandtab
