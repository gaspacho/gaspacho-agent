# Copyright (C) 2012 Emmanuel Garette <gnunux@gnunux.info>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

from os.path import join, isfile
from subprocess import Popen, PIPE

from gaspacho_agent.util import logger, trace

gasp_version = "0.1"

class Apply:
    @trace
    def __init__(self, conflevel):
        self.conflevel = conflevel

    @trace
    def is_compatible(self):
        return True

    @trace
    def initialize(self):
        self.path = None

    @trace
    def open(self, path):
        if self.path != None:
            raise Exception('use close() before it')
        if not isfile(path):
            raise Exception('{0} not found'.format(self.path))
        self.path = path
        self.cmd = [self.path]

    @trace
    def suppr(self, key, info):
        if self.path == None:
            raise Exception('use open() before it')
        self.info = None

    @trace
    def set(self, key, value, info):
        if self.path == None:
            raise Exception('use open() before it')
        if value != None:
            typ = type(value)
            if typ in [unicode, int]:
                value = str(value)
            else:
                raise TypeError('unknown type: %s ' % str(typ))
            self.cmd.append('{0}={1}'.format(key, value))
        else:
            self.cmd.append('{0}'.format(key))
        if info != None:
            self.cmd.extend(info.split())

    @trace
    def close(self):
        if self.path == None:
            raise Exception('already close')
        if self.cmd != None:
            logger.debug('launch command %s' % self.cmd)
            process = Popen(self.cmd, stdout=PIPE, stderr=PIPE, stdin=PIPE)
            stdout, stderr = process.communicate()
            logger.debug(stdout)
            if process.returncode:
                logger.error('error in %s: %s' % (self.cmd, stderr))
        else:
            logger.debug('exec cancel')

        self.path = None

    @trace
    def finalize_without_target(self):
        pass

    @trace
    def finalize(self):
        if self.path != None:
            raise Exception('use close() before it')

# vim: ts=4 sw=4 expandtab
