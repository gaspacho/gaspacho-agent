# Copyright (C) 2012 Laurent Flori <laurent.flori@ac-dijon.fr>
#               2012 Emmanuel Garette <gnunux@gnunux.info>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

import sys
from os.path import join, split
from os import system

from gaspacho_agent.util import logger, trace

compatible = True
try:
    import gconf
except:
    logger.debug('')
    compatible = False
try:
    sys.path.append('/usr/lib/gnome-panel')
    from gnomepaneladd import PanelAdder
except:
    logger.debug("need script /usr/lib/gnome-panel/gnomepaneladd.py")
    compatible = False

OBJECTS_PATH='/apps/panel/objects'
APPLETS_PATH='/apps/panel/applets'

gasp_version = "0.1"

class Apply:
    """Support Launcher and applet for GNOME 2"""
    @trace
    def __init__(self, conflevel):
        self.conflevel = conflevel
        self.panel_need_restart = False

    @trace
    def is_compatible(self):
        if self.conflevel != 'context':
            return False
        return compatible

    @trace
    def initialize(self):
        self.path = None
        self.gconf_client = gconf.client_get_default()
        self.results = {'applet': {}, 'launcher': {}}
        self.get_list(what = 'applet')
        self.get_list(what = 'launcher')

    @trace
    def open(self, path):
        if self.path != None:
            raise Exception('use close() before it')
        self.path = path

    def get_list(self, what):
        logger.debug('get list for %s' % what)
        if what == 'applet':
            start_path = APPLETS_PATH
        else:
            start_path = OBJECTS_PATH

        for dirs in self.gconf_client.all_dirs(start_path):
            if what == 'applet':
                try:
                    key = self.gconf_client.get_value(
                                    dirs+'/bonobo_iid').replace('OAFIID:', '')
                except:
                    key = dirs.replace(start_path+'/','')
            elif what == 'launcher':
                try:
                    key = self.gconf_client.get_value(
                                    dirs+'/launcher_location')
                except:
                    key = dirs.replace(start_path+'/','')
            else:
                key = dirs.replace(start_path+'/','')

            self.results[what][key] = {}
            self.results[what][key]['internal_name'] = split(dirs)[1]
            if self.gconf_client.all_entries(dirs) is not None:
                for entries in self.gconf_client.all_entries(dirs):
                    if entries.get_value() is not None:
                        self.results[what][key][entries.get_key().replace(
                                dirs+'/', '')] = entries.get_value().to_string()
        logger.debug(self.results[what])

    @trace
    def set(self, key, value, info):
        if self.path == None:
            raise Exception('use open() before it')

        toplevel_panel = 'top_panel_screen0'
        toplevel_list = self.gconf_client.get_list('/apps/panel/general/toplevel_id_list', 'string')

        if len(toplevel_list) != 0 and toplevel_panel not in toplevel_list:
            #Si le panel du haut n'existe pas on prend le premier qu'on trouve
            toplevel_panel = toplevel_list[0]

        if info == 'applet':
            if not self.results['applet'].has_key(key):
                obj = PanelAdder(toplevel_panel, -1, False,
                                    applet_iid = 'OAFIID:'+key)
            else:
                logger.debug("Applet already added")
                return
        else:
            nkey = 'file://%s.desktop' % join(self.path, key)
            if not self.results['launcher'].has_key(nkey):
                obj = PanelAdder(toplevel_panel, -1, False,
                            launcher_path = join(self.path, key+'.desktop'))
            else:
                logger.debug('Launcher already added')
                return
        obj.run()

    @trace
    def close(self):
        if self.path == None:
            raise Exception('already close')
        self.path = None

    @trace
    def suppr(self, key, info):
        #Suppression d'un objet
        if self.path == None:
            raise Exception('use open() before it')
        if info == None:
            raise Exception('info needed for this plugin')
        if info == 'applet':
            start_path = APPLETS_PATH
        else:
            start_path = OBJECTS_PATH
        
        if self.results[info].has_key(key):
            path_to_remove = start_path+'/'+self.results[info][key]['internal_name']
            self.gconf_client.recursive_unset(path_to_remove, True)
            self.gconf_client.unset(path_to_remove)
            self.gconf_client.remove_dir(path_to_remove)
            self.gconf_client.clear_cache()
            self.panel_need_restart = True
        else:
            logger.debug('already suppress')

    @trace
    def finalize_without_target(self):
        pass

    @trace
    def finalize(self):
        if self.path != None:
            raise Exception('use close() before it')
        if self.panel_need_restart == True:
            system('/usr/bin/killall gnome-panel')

# vim: ts=4 sw=4 expandtab
