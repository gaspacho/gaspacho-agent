# Copyright (C) 2010 Emmanuel Garette <gnunux@gnunux.info>
#                    Antoine Roger
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

from os.path import isdir, join, isfile
from os import environ
from subprocess import Popen, PIPE
from shutil import rmtree

from gaspacho_agent.plugins.gasp_ini import Ini
from gaspacho_agent.util import makedirs
from gaspacho_agent.util import logger, trace

UID = str(environ['UID'])
DCONF_BIN = '/usr/bin/dconf'
DCONF_DIR = '/etc/dconf'
DCONF_DB_PATH = join(DCONF_DIR, 'db', '%s.d' % UID)
DCONF_PROFILE_PATH = join(DCONF_DIR, 'profile', UID)
DCONF_LOCK_PATH = join(DCONF_DB_PATH, 'locks')

gasp_version = "0.1"

class Apply:
    @trace
    def __init__(self, conflevel):
        self.conflevel = conflevel

    def is_compatible(self):
        if self.conflevel != 'user':
            return False
        if not isdir(DCONF_DIR):
            return False
        if not isfile(DCONF_BIN):
            return False
        return True

    @trace
    def clean(self):
        if isdir(DCONF_DB_PATH):
            rmtree(DCONF_DB_PATH)
        if isdir(DCONF_PROFILE_PATH):
            rmtree(DCONF_PROFILE_PATH)

    @trace
    def initialize(self):
        self.clean()
        if not isdir(DCONF_LOCK_PATH):
            makedirs(DCONF_LOCK_PATH, mode=0755)
        #initialize config dir
        file_profile = open(DCONF_PROFILE_PATH, 'w')
        file_profile.write('user\n')
        file_profile.write('%s\n'%UID)
        file_profile.close()
        #empty ini file
        ini = open(join(DCONF_LOCK_PATH, 'gaspacho'), 'w')
        ini.write('')
        ini.close()
        self.ini = Ini(user_owner=False)
        self.ini.open(join(DCONF_DB_PATH, 'gaspacho'))
        self.locksfile = open(join(DCONF_LOCK_PATH, 'gaspacho'), 'w')
        self.locksfile.write('')
        self.path = None

    @trace
    def open(self, path):
        if self.path != None:
            raise Exception('use close() before it')
        self.path = path

    @trace
    def suppr(self, key, info):
        if self.path == None:
            raise Exception('use open() before it')
        if info != None:
            raise ValueError('info must not be set')
        del(self.ini[self.path][key])

    @trace
    def set(self, key, value, info):
        if self.path == None:
            raise Exception('use open() before it')
        if info != None:
            raise ValueError('info must not be set')
        self.locksfile.write('%s\n'%join(self.path, key))
        path = self.path
        if path.startswith('/'):
            path = path[1:]
        if path.endswith('/'):
            path = path[:-1]
        if type(value) == unicode:
            value = str(value)
        self.ini[path][key] = value

    @trace
    def close(self):
        if self.path == None:
            raise Exception('already close')
        self.path = None

    @trace
    def finalize_without_target(self):
        self.finalize()

    @trace
    def finalize(self):
        if self.path != None:
            raise Exception('use close() before it')
        self.ini.close()
        self.locksfile.close()
        logger.debug('update dconf')
        cmd = [DCONF_BIN, 'update']
        process = Popen(cmd, stdout=PIPE, stderr=PIPE, stdin=PIPE)
        stdout, stderr = process.communicate()
        logger.debug(stdout)
        if process.returncode:
            logger.debug('error in %s: %s' % (' '.join(cmd), stderr))

# vim: ts=4 sw=4 expandtab
